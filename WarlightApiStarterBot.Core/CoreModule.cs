﻿using Ninject.Modules;
using WarlightApiStarterBot.Core.Concrete;
using WarlightApiStarterBot.Services.Contracts;

namespace WarlightApiStarterBot.Core
{
    public class CoreModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConfiguration>().To<Configuration>();
        }
    }
}
