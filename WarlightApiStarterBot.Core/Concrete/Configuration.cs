﻿using System.Collections.Specialized;
using System.Configuration;
using WarlightApiStarterBot.Services.Contracts;

namespace WarlightApiStarterBot.Core.Concrete
{
    public class Configuration : IConfiguration
    {
        public Configuration(): this(ConfigurationManager.AppSettings)
        {
        }
        internal Configuration(NameValueCollection appSettings)
        {
            ApiKey = appSettings["ApiKey"];
        }
        public string ApiKey { get; private set; }
    }
}
