﻿using WarlightApiStarterBot.Services.Constants;

namespace WarlightApiStarterBot.Services.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        public static string AppendUrl(this string text, string endpoint)
        {
            return string.Format(FormattingConstants.APPEND_URL_FORMAT, text, endpoint);
        }
    }
}
